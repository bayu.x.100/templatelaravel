@extends('adminlte.master')

@section('content')
    <form role="form" action="/posts/{{$post->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="card-body">
            <div class="card-header">
                <h3 class="card-title">Edit Post {{$post->id}} </h3>
            </div>
            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" class="form-control" id="title" name="title" value=" {{ old('title', $post->title) }} " placeholder="Enter Title">
                @error('title')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">Body</label>
                <input type="text" class="form-control" id="body" name="body" value=" {{ old('body', $post->body)}}" placeholder="body">
                @error('title')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
        <button type="submit" class="btn btn-primary">Edit</button>
        </div>
    </form>
@endsection