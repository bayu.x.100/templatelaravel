@extends('adminlte.master')

@section('content')
    <form role="form" action="/posts" method="POST">
        @csrf
        <div class="card-body">
            <div class="card-header">
                <h3 class="card-title">Create New Post</h3>
            </div>
            <div class="form-group">
                <label for="title">Judul</label>
                <input type="text" class="form-control" id="title" name="title" value=" {{ old('title', '') }} " placeholder="Enter Title">
                @error('title')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">Isi</label>
                <input type="text" class="form-control" id="body" name="body" value=" {{ old('body', '')}}" placeholder="body">
                @error('title')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
        <button type="submit" class="btn btn-primary">Buat</button>
        </div>
    </form>
@endsection