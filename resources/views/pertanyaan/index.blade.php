@extends('adminlte.master')

@section('content')
<div class="card">
    <div class="card-header">
      <h3 class="card-title">Tabel Pertanyaan</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
        <a class="btn btn-primary mb-2" href="/posts/create">Create New Post</a>
      <table class="table table-bordered">
        <thead>                  
          <tr>
            <th style="width: 10px">#</th>
            <th>Judul</th>
            <th>Isi</th>
            <th>Tanggal Dibuat</th>
            <th>Tanggal Diperbaharui</th>
            <th style="width: 40px">Action</th>
          </tr>
        </thead>
        <tbody>
            @forelse ($pertanyaan as $key => $value)
              <tr>
                  <td> {{$key + 1}} </td>
                  <td> {{$value->judul}} </td>
                  <td> {{$value->isi}} </td>
                  <td> {{$value->tanggal_dibuat}} </td>
                  <td> {{$value->tanggal_diperbaharui}} </td>
                  <td style="display: flex">
                    <a href="/posts/{{$value->id}}" class="btn btn-info btn-sm mr-1">show</a>
                    <a href="/posts/{{$value->id}}/edit" class="btn btn-default btn-sm mr-1">edit</a>
                    <form action="/posts/{{$value->id}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="submit" value="delete" class="btn btn-danger btn-sm">
                    </form>
                    </td>
              </tr>
            @empty
                <tr colspan="3">
                    <td colspan="4">No data</td>
                </tr>  
            @endforelse
        </tbody>
      </table>
    </div>
    <!-- /.card-body -->
  </div>
@endsection