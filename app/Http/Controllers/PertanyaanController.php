<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    public function create() {
        return view('pertanyaan.create');
    }

    public function store(Request $request) {
        $request->validate([
            'judul' => 'required|unique:post',
            'isi' => 'required',
        ]);

        $query = DB::table('pertanyaan')->insert([
            "judul" => $request["title"],
            "isi" => $request["body"]
        ]);
        return redirect('/pertanyaan')->with('success', 'Pertanyaan tersimpan');
    }

    public function index() {
        $post = DB::table('pertanyaan')->get();
        return view ('pertanyaan.index', compact('pertanyaan'));
    }

    public function show($id) {
        $post = DB::table('pertanyaan')->where('id', $id)->first();
        // dd($post);
        return view ('pertanyaan.show', compact('pertanyaan'));
    }

    public function edit($id) {
        $post = DB::table('pertanyaan')->where('id', $id)->first();
        // dd($post);
        return view ('pertanyaan.edit', compact('pertanyaan'));
    }

    public function update($id, Request $request) {
        $request->validate([
            'title' => 'required|unique:post',
            'body' => 'required'
        ]);

        $query = DB::table('pertanyaan')
        ->where('id', $id)
        ->update([
            'title' => $request["title"],
            'body' => $request["body"]
        ]);
    return redirect('/pertanyaan')->with('success', 'berhasil update pertanyaan');
    }

    public function destroy($id) {
        $query = DB::table('pertanyaan')->where('id', $id)->delete();
        return redirect('/pertanyaan')->with('success', 'post pertanyaan terhapus');
    }
}
