<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\DB as FacadesDB;

class PostController extends Controller
{
    public function create() {
        return view('posts.create');
    }

    public function store(Request $request) {
        $request->validate([
            'title' => 'required|unique:post',
            'body' => 'required'
        ]);

        $query = DB::table('post')->insert([
            "title" => $request["title"],
            "body" => $request["body"]
        ]);
        return redirect('/posts')->with('success', 'Post tersimpan');
    }

    public function index() {
        $post = DB::table('post')->get();
        return view ('posts.index', compact('post'));
    }

    public function show($id) {
        $post = DB::table('post')->where('id', $id)->first();
        // dd($post);
        return view ('posts.show', compact('post'));
    }

    public function edit($id) {
        $post = DB::table('post')->where('id', $id)->first();
        // dd($post);
        return view ('posts.edit', compact('post'));
    }

    public function update($id, Request $request) {
        $request->validate([
            'title' => 'required|unique:post',
            'body' => 'required'
        ]);

        $query = DB::table('post')
        ->where('id', $id)
        ->update([
            'title' => $request["title"],
            'body' => $request["body"]
        ]);
    return redirect('/posts')->with('success', 'berhasil update post');
    }

    public function destroy($id) {
        $query = DB::table('post')->where('id', $id)->delete();
        return redirect('/posts')->with('success', 'post terhapus');
    }
}
